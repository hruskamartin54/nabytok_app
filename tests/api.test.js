var webdriver = require("selenium-webdriver");
var firefox = require("selenium-webdriver/firefox");
var assert = require("chai").assert;
var expect = require("chai").expect;

var driver = new webdriver.Builder()
    .forBrowser("firefox")
    .setFirefoxOptions(/* … */)
    .build();

var service = "http://localhost:4000";

const generateRandomId = () => {
    var text = "";
    var possible =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 5; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
};

describe("Main Test Suite", function() {
    // set a global timeout for tests
    this.timeout(20000);

    // open the app before each test
    before(() => {
        driver.get(service);
    });

    // close the browser after all tests are done
    after(() => {
        return driver.quit();
    });

    describe("The main application page", function() {
        it("mounts correctly", function(done) {
            driver.getTitle().then(title => {
                expect(title).equals("Nábytková firma");
                done();
            });
        });
    });

    describe("form for ordering", function() {
        it("submits an order and returns success response", function(done) {
            const randomIdentifierForTest = generateRandomId();

            driver
                .findElement(webdriver.By.id("udaje"))
                .sendKeys(randomIdentifierForTest)
                .then(() => {
                    driver.findElement(webdriver.By.id("submit_order")).click();
                    done();
                });
        });
    });
});
