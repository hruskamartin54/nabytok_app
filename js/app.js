function ulozitDoSkladu(udaje, vysledok) {
  var data = JSON.stringify(udaje);

  $.ajax({
    url: "http://localhost:3000/objednavky",
    type: "POST",
    data: data,
    contentType: "application/json",
    success: function(res) {
      vysledok(res);
    },
    error: function(error) {
      console.error(error);
    }
  });
}

function vybratObjednavku(vysledok) {
  $.ajax({
    url: "http://localhost:3000/vybratObjednavku",
    type: "GET",
    success: function(res) {
      vysledok(res);
    },
    error: function(error) {
      console.error(error);
    }
  });
}

function hodnotaZoSkladu(typ, vysledok) {
  $.ajax({
    url: "http://localhost:3000/" + typ,
    type: "GET",
    success: function(res) {
      vysledok(res);
    },
    error: function(error) {
      console.error(error);
    }
  });
}

function ukazatOznam(odpoved) {
  if (odpoved) {
    console.log("vsetko prebehlo v poriadku");
  } else {
    console.log("niekde nastala chyba");
  }
}

function vybratObjednavkuPodlaMena(meno, cb) {
  $.ajax({
    type: "GET",
    url: "http://localhost:3000/objednavka/" + meno,
    success: function(res) {
      cb(res);
    },
    error: function(e) {
      console.error(e);
    }
  });
}

$(document).ready(function() {
  hodnotaZoSkladu("objednavky", function(res) {
    $("#objednavkysklad").val(res.objednavky);
  });

  hodnotaZoSkladu("skrinky", function(res) {
    $("#skrinkysklad").val(res.skrinky);
  });

  hodnotaZoSkladu("drevo", function(res) {
    $("#drevoSklad").val(res.drevo);
  });

  vybratObjednavku(function(vysledok) {
    for (var i = 0; i < vysledok.objednavky.length; i++) {
      var objednavka = vysledok.objednavky[i];

      $("#listObjednavok").append("<option>" + objednavka.udaje + "</option>");
    }
  });
});

$("#objednavkovaForma").on("submit", function(event) {
  // prevencia skocenia uplne hore po odoslani formy
  event.preventDefault();

  // extrakcia vsetkych udajov z formularu
  var udaje = $("#objednavkovaForma :input");

  // transformacia dat z pola udajov do asociacneho objektu
  var objekt = {};
  udaje.each(function() {
    objekt[this.name] = this.value;
  });

  // ulozenie objektu udajov na serveri
  ulozitDoSkladu(objekt, ukazatOznam);
});

$("#listObjednavok").on("change", function(e) {
  vybratObjednavkuPodlaMena(e.target.value, function(res) {
    var udaje = res.data;
    $("#MP").val(udaje.udaje);
    $("#AT").val(udaje.adresa);
    $("#MOM").val(udaje.material);
    $("#TD").val(udaje.dekor);
    $("#MOS").val(udaje.skrinky);
  });
});
